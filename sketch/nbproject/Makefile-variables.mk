#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug configuration
CND_PLATFORM_Debug=GNU-Linux
CND_ARTIFACT_DIR_Debug=dist/Debug
CND_ARTIFACT_NAME_Debug=basic
CND_ARTIFACT_PATH_Debug=dist/Debug/basic
CND_PACKAGE_DIR_Debug=dist/Debug/package
CND_PACKAGE_NAME_Debug=basic.tar
CND_PACKAGE_PATH_Debug=dist/Debug/package/basic.tar
# Release configuration
CND_PLATFORM_Release=GNU-Linux
CND_ARTIFACT_DIR_Release=dist/Release
CND_ARTIFACT_NAME_Release=terminal-basic
CND_ARTIFACT_PATH_Release=dist/Release/terminal-basic
CND_PACKAGE_DIR_Release=dist/Release/GNU-Linux/package
CND_PACKAGE_NAME_Release=sketch.tar
CND_PACKAGE_PATH_Release=dist/Release/GNU-Linux/package/sketch.tar
# Arduino_nano328 configuration
CND_PLATFORM_Arduino_nano328=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_nano328=dist/Arduino_nano328
CND_ARTIFACT_NAME_Arduino_nano328=basic
CND_ARTIFACT_PATH_Arduino_nano328=dist/Arduino_nano328/basic
CND_PACKAGE_DIR_Arduino_nano328=dist/Arduino_nano328/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_nano328=sketch.tar
CND_PACKAGE_PATH_Arduino_nano328=dist/Arduino_nano328/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_UNO configuration
CND_PLATFORM_Arduino_UNO=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_UNO=dist/Arduino_UNO
CND_ARTIFACT_NAME_Arduino_UNO=terminal-basic
CND_ARTIFACT_PATH_Arduino_UNO=dist/Arduino_UNO/terminal-basic
CND_PACKAGE_DIR_Arduino_UNO=dist/Arduino_UNO/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_UNO=sketch.tar
CND_PACKAGE_PATH_Arduino_UNO=dist/Arduino_UNO/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_mega2560 configuration
CND_PLATFORM_Arduino_mega2560=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_mega2560=dist/Arduino_mega2560
CND_ARTIFACT_NAME_Arduino_mega2560=terminal-basic
CND_ARTIFACT_PATH_Arduino_mega2560=dist/Arduino_mega2560/terminal-basic
CND_PACKAGE_DIR_Arduino_mega2560=dist/Arduino_mega2560/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_mega2560=sketch.tar
CND_PACKAGE_PATH_Arduino_mega2560=dist/Arduino_mega2560/Arduino-1.8.6-Linux/package/sketch.tar
# Atmega_128a configuration
CND_PLATFORM_Atmega_128a=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Atmega_128a=dist/Atmega_128a
CND_ARTIFACT_NAME_Atmega_128a=basic
CND_ARTIFACT_PATH_Atmega_128a=dist/Atmega_128a/basic
CND_PACKAGE_DIR_Atmega_128a=dist/Atmega_128a/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Atmega_128a=sketch.tar
CND_PACKAGE_PATH_Atmega_128a=dist/Atmega_128a/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_nano168 configuration
CND_PLATFORM_Arduino_nano168=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_nano168=dist/Arduino_nano168
CND_ARTIFACT_NAME_Arduino_nano168=basic
CND_ARTIFACT_PATH_Arduino_nano168=dist/Arduino_nano168/basic
CND_PACKAGE_DIR_Arduino_nano168=dist/Arduino_nano168/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_nano168=sketch.tar
CND_PACKAGE_PATH_Arduino_nano168=dist/Arduino_nano168/Arduino-1.8.6-Linux/package/sketch.tar
# Atmega1284 configuration
CND_PLATFORM_Atmega1284=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Atmega1284=dist/Atmega1284
CND_ARTIFACT_NAME_Atmega1284=basic.elf
CND_ARTIFACT_PATH_Atmega1284=dist/Atmega1284/basic.elf
CND_PACKAGE_DIR_Atmega1284=dist/Atmega1284/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Atmega1284=sketch.tar
CND_PACKAGE_PATH_Atmega1284=dist/Atmega1284/Arduino-1.8.6-Linux/package/sketch.tar
# Atmega644 configuration
CND_PLATFORM_Atmega644=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Atmega644=dist/Atmega644
CND_ARTIFACT_NAME_Atmega644=basic.elf
CND_ARTIFACT_PATH_Atmega644=dist/Atmega644/basic.elf
CND_PACKAGE_DIR_Atmega644=dist/Atmega644/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Atmega644=sketch.tar
CND_PACKAGE_PATH_Atmega644=dist/Atmega644/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_pro328 configuration
CND_PLATFORM_Arduino_pro328=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_pro328=dist/Arduino_pro328
CND_ARTIFACT_NAME_Arduino_pro328=basic
CND_ARTIFACT_PATH_Arduino_pro328=dist/Arduino_pro328/basic
CND_PACKAGE_DIR_Arduino_pro328=dist/Arduino_pro328/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_pro328=sketch.tar
CND_PACKAGE_PATH_Arduino_pro328=dist/Arduino_pro328/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_pro328p configuration
CND_PLATFORM_Arduino_pro328p=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_pro328p=dist/Arduino_pro328p
CND_ARTIFACT_NAME_Arduino_pro328p=basic
CND_ARTIFACT_PATH_Arduino_pro328p=dist/Arduino_pro328p/basic
CND_PACKAGE_DIR_Arduino_pro328p=dist/Arduino_pro328p/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_pro328p=sketch.tar
CND_PACKAGE_PATH_Arduino_pro328p=dist/Arduino_pro328p/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_pro168 configuration
CND_PLATFORM_Arduino_pro168=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_pro168=dist/Arduino_pro168
CND_ARTIFACT_NAME_Arduino_pro168=basic
CND_ARTIFACT_PATH_Arduino_pro168=dist/Arduino_pro168/basic
CND_PACKAGE_DIR_Arduino_pro168=dist/Arduino_pro168/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_pro168=sketch.tar
CND_PACKAGE_PATH_Arduino_pro168=dist/Arduino_pro168/Arduino-1.8.6-Linux/package/sketch.tar
# Arduino_pro88 configuration
CND_PLATFORM_Arduino_pro88=Arduino-1.8.6-Linux
CND_ARTIFACT_DIR_Arduino_pro88=dist/Arduino_pro88
CND_ARTIFACT_NAME_Arduino_pro88=basic
CND_ARTIFACT_PATH_Arduino_pro88=dist/Arduino_pro88/basic
CND_PACKAGE_DIR_Arduino_pro88=dist/Arduino_pro88/Arduino-1.8.6-Linux/package
CND_PACKAGE_NAME_Arduino_pro88=sketch.tar
CND_PACKAGE_PATH_Arduino_pro88=dist/Arduino_pro88/Arduino-1.8.6-Linux/package/sketch.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
