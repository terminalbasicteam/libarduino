void
setup()
{
	Serial.begin(115200);
	
	pinMode(LED_BUILTIN, OUTPUT);
	pinMode(3, OUTPUT);
	
	digitalWrite(LED_BUILTIN, HIGH);
	
	tone(3, 440, 1000);
}

void
loop()
{
	Serial.println(__PRETTY_FUNCTION__);
}
